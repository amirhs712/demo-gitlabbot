<?php

namespace App\Scripts\Enums;


use App\Models\Admission;
use App\Scripts\Validation\CustomException;
use Illuminate\Validation\Rule;
use RuntimeException;

/**
 * Class ValidationEnum
 * @description
 * Responsible for generating validation rules.
 * @package App\Scripts\Enums
 */
class ValidationEnum
{
    public const MOBILE = 'digits:11|regex:' . RegexEnum::MOBILE;
    public const HID = 'string|min:5|max:19';
    public const URL = 'url';
    public const CARD_NUMBER = 'string|size:16';
    public const TEL = 'digits:11';
    public const SIAM = 'string|size:36';
    public const IRC = 'string|max:100';
    public const NATIONAL_CODE = 'required|digits:10';
    public const INT = 'integer|max:2147483647|min:0';
    public const SMALLINT = 'integer|min:0|max:65535';
    public const TINYINT = 'integer|min:0|max:99';
    public const BOOLEAN = 'boolean';
    public const NUMERIC = 'numeric|max:2147483647';
    public const ARRAY = 'array';
    //---- Foreign keys
    public const ID = 'required|integer|max:2147483647|min:0|';
    public const USER_NODE_ID = 'integer|max:2147483647|min:0|exists:com.node_user,id,deleted_at,NULL';
    public const USER = 'integer|max:2147483647|min:0|exists:com.users,id';
    public const ROLE = 'integer|max:2147483647|min:0|exists:com.roles,id';
    public const PERMISSION = 'integer|max:2147483647|min:0|exists:com.permission,id';
    public const NODE_ID = 'integer|max:2147483647|min:0|exists:com.nodes,id';
    public const REF_NODE_ID = 'integer|max:2147483647|min:0|exists:com.nodes,id,type,0,parent_id,NULL';
    public const PARENT_NODE = 'integer|max:2147483647|min:0|exists:com.nodes,id,parent_id,NULL';
    public const SPECIALITY_ID = 'integer|max:2147483647|min:0|exists:com.specialities,id';
    public const CLIENT_ID = 'integer|max:2147483647|min:0|exists:com.clients,id';
    public const INSURANCE_BOX = 'integer|max:2147483647|min:0|exists:com.insurance_boxes,id';
    public const INSURANCE = 'integer|max:2147483647|min:0|exists:com.insurances,id';
    public const SERVICE = 'integer|max:2147483647|min:0|exists:com.services,id';
    public const SERVICE_GROUP = 'integer|max:2147483647|min:0|exists:com.service_groups,id';
    public const RADIO_SERVICE = 'string|max:10|exists:com.radio_services,id';
    public const DR_SERVICE = 'integer|max:2147483647|min:0|exists:com.dr_services,id';
    public const SPECIALITY = 'integer|max:2147483647|min:0|exists:com.specialities,id';
    public const ADMISSION_ID = 'integer|max:2147483647|min:0|exists:admissions,id';
    public const COMPLAINT_ID = 'string|exists:com.complaints,id';
    public const FAVORITE = 'integer|max:2147483647|min:0|exists:com.favorites,id';
    public const NET_UID = 'integer|max:2147483647|min:0|exists:com.net_uids,id';
    public const TOOTH = 'integer|max:2147483647|min:0|exists:com.teeth,id';
//    public const DENTAL_ACTION = 'integer|max:2147483647|min:0|exists:com.dental_action,id';
//    public const DENTAL_ACTION_DIAGNOSIS = 'integer|max:2147483647|min:0|exists:dental_actions,id,type,DIAGNOSIS';
//    public const DENTAL_ACTION_TREATMENT = 'integer|max:2147483647|min:0|exists:dental_actions,id,type,TREATMENT';

    public const ILLNESS_ID = 'string|max:10|exists:illnesses,id';
    public const FDO = 'string|max:10|exists:fdo,id';
    public const MEDICATION_ID = 'required|string|max:10|exists:fdo,id';
//    public const TQ_ID = 'string|max:100';

    public const S_SERVICE = 'integer|max:2147483647|exists:s_services,code';
    public const S_PRODUCT = 'integer|max:2147483647|exists:s_products,code';
    public const AD_SERVICE = 'integer|max:2147483647|min:0|exists:admission_services,id,deleted_at,NULL';

    //------------End of foreign keys

    //Dates
    public const YEAR = 'digits:4';
    public const JDATE = 'string|size:10|jdate';
    public const JDATE_PAST = 'string|size:10|jdate,now,le';
    public const JDATENOW = 'string|size:10|jdate:now';
    public const TIME = 'string|size:8|date_format:H:i:s';
    public const JDATETIME = 'string|size:19|jdatetime';
    public const JDATETIME_NOW = 'string|size:19|jdatetime:now';
    public const JDATETIME_PAST = 'string|size:19|jdatetime:now,le';
    public const DAYOFWEEK = 'integer|min:0|max:6';

    //Others
    public const STRING = 'string|max:200';
    public const EMPTY_RULE = '';
    public const SMALLTEXT = 'string|max:1000';
    public const TEXT = 'string|max:3000';

    public const CUSTOM_CODE = 'string|max:15';
    public const CONSUMPTION = 'string|size:1|regex:/[A-V]/';
    public const CONSUMPTION_INS = 'string|size:1|regex:/[A-L]/';

    /**
     * @param string $field
     * @param string $rule
     * @param bool $required
     * @return array
     */
    public static function ids($field = 'ids', $rule = 'required|integer|max:2147483647|min:0', $required = true)
    {
        return [
            $field => self::get('array', $required),
            "$field.*" => $rule
        ];
    }

    /**
     * @param $param
     * @param bool $required
     * @param string $extra
     * @return string
     */
    public static function get($param, $required = true, $extra = '')
    {
        $param = strtoupper($param);
        $prefix = self::baseRule($required);
        $rule = $prefix . constant("self::$param");

        return self::appendExtraRule($rule, $extra);
    }

    /**
     * @param bool $required
     * @return string
     */
    private static function baseRule($required = true)
    {
        return $required ? 'required|' : 'sometimes|nullable|';
    }

    /**
     * @param bool $required
     * @return array
     */
    public static function hid_rule($required = false)
    {
        if ($required) {
            return [
                'required', 'string', 'min:5', 'max:19',
                'regex:' . RegexEnum::HID
            ];
        }

        return [
            'sometimes', 'nullable', 'string', 'min:5', 'max:19',
            'regex:' . RegexEnum::HID
        ];
    }

    /**
     * @return array
     */
    public static function status()
    {
        return [
            'required', 'string', Rule::in([Enum::ACTIVE, Enum::BANNED, Enum::DEACTIVATED])
        ];
    }

    //TODO: all of these could've been much better if it was more object oriented, we should've had RequiredGenerator, ruleConcatenater and so on...
    //The Rules String or Array should've been an object called RuleFactory so that you could've modified it easily.

    /**
     * @return array
     */
    public static function methods()
    {
        return [
            'required', 'string', Rule::in(Enum::GET, Enum::POST, Enum::PUT, Enum::PATCH, Enum::DELETE)
        ];
    }

    /**
     * @param array $array
     * @param bool $required
     * @param string|null $rule
     * @return array
     */
    public static function inArray(array $array, $required = false, string $rule = null)
    {
        $return = [];
        if ($rule) {
            $return = explode('|', self::get($rule, $required));
        }
        if ($rule == null && $required) {
            $return[] = 'required';
        }
        $return[] = Rule::in($array);

        return $return;
    }

    /**
     * @param int $chars
     * @param bool $required
     * @param string $extra
     * @return string
     */
    public static function stringOf($chars = 30, $required = true, $extra = '')
    {
        $rule = self::baseRule($required) . "max:$chars";
        return self::appendExtraRule($rule, $extra);
    }

    /**
     * @param string $type
     * @return array
     */
    public static function fromTo($type = 'node_id')
    {
        return [
            'from' => self::get('jdate'),
            'to' => self::get('jdate'),
            $type => self::get($type)
        ];
    }

    /**
     * @param $type
     * @return array
     */
    public static function enum($type)
    {
        switch ($type) {
            case 'area':
            {
                return [
                    'country_id' => 'sometimes|nullable|string|max:50',
                    'province_id' => 'sometimes|nullable|string|max:50',
                    'city_id' => 'sometimes|nullable|string|max:50',
                    'district_id' => 'sometimes|nullable|string|max:50',
                    'rural_area_id' => 'sometimes|nullable|string|max:50',
                    'village_id' => 'sometimes|nullable|string|max:50',
                ];
            }
            default:
                throw new RuntimeException("Enum $type is not defined.");
        }
    }

    public static function foreign($table, $required = true, $field = 'id', $extra = '')
    {
        $rule = self::baseRule($required) . "exists:$table,$field";
        return self::appendExtraRule($rule, $extra, ',');
    }


    public static function coding(int $type, $required = true, $extra = '')
    {
        $rule = self::foreign('codings', $required, 'id', "terminology,$type");
        return self::appendExtraRule($rule, $extra);
    }

    private static function appendExtraRule(string $rule, string $extra, string $delimiter = '|')
    {
        return $extra ? $rule . $delimiter . $extra : $rule;
    }
}
