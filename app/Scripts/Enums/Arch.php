<?php /** @noinspection CallableParameterUseCaseInTypeContextInspection */

/**
 * Created by PhpStorm.
 * Date: 2018-11-13
 * Time: 9:22 PM
 */

namespace App\Scripts\Enums;


use DB;
use Illuminate\Http\JsonResponse;
use stdClass;

/**
 * Class Arch
 * @package App\Scripts\Enums
 */
class Arch
{
    protected static $queryLogEnabled = false;

    public static function enableQueryLog()
    {
        if (config('app.debug') === true) {
            self::$queryLogEnabled = true;
            DB::enableQueryLog();
        }
    }

    /**
     * @param $message
     * @return JsonResponse
     */
    public static function validation($message)
    {
        return self::api(422, $message);
    }

    public static function login()
    {
        return self::api(-100, 'Invalid token, login required.');
    }

    /**
     * @param int $status
     * @param string $message
     * @param array $data
     * @param null $values
     * @param int $httpStatus
     * @return JsonResponse
     */
    public static function api($status = 1, $message = null, $data = [], $values = null, $httpStatus = 200)
    {

        if ($values != null) {
            foreach ($values as $key => $value) {
                $data[$key] = $value;
            }
        }

        if ($message == null) {
            if ($status && $status > 0) {
                $message = 'Success';
            } else {
                $message = 'Failed';
            }
        }

        $body = [
            'status' => (int)$status,
            'message' => $message,
            'data' => $data,
        ];

        if (self::$queryLogEnabled) {
            $body['queryLog'] = DB::getQueryLog();
            $body['queryLog']['QueryCount'] = count($body['queryLog']);
        }

        return response()->json($body, $httpStatus);
    }

    public static function resource($data, $status = 1, $message = null, $wrapper = null)
    {
        if ($wrapper) {
            $out[$wrapper] = $data;
        } else {
            $out = $data;
        }
        return self::api($status, $message, $out);
    }

    /**
     * @param string $url
     * @return JsonResponse
     */
    public static function redirect($url)
    {
        return self::api(300, 'Redirect', compact('url'));
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public static function id($id)
    {
        return self::api(1, '', compact('id'));
    }


}
