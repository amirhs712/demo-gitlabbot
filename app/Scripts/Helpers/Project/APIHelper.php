<?php


namespace App\Scripts\Helpers\Project;


use App\Scripts\Validation\CustomException;
use Gitlab\Client;
use Gitlab\Exception\RuntimeException;

class APIHelper
{
    public static function getClient()
    {
        static $client;
        if ($client) {
            return $client;
        }

        abort_if(!auth()->check(), 401);

        $user = user();
        cexception_if(!$user->gitlab_token, CustomException::GITLAB_TOKEN_NOT_SET);

        $client = (new Client())->authenticate($user->gitlab_token, Client::AUTH_HTTP_TOKEN);

        return $client;
    }

    public static function getMe()
    {
        $client = self::getClient();
        return $client->users()->me();
    }
}
