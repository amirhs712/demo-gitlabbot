<?php


namespace App\Scripts\Helpers\Project;


use Carbon\Carbon;
use Illuminate\Support\Arr;

class IssueTimeTrackingHelper
{
    public static function report()
    {
        $client = APIHelper::getClient();

        //Step1. Get all project
        //Step2. Get all issues for each project
        //Step3. Generate monthly and daily reports for time stats.

        $userProjects = $client->projects()->all(['membership' => true]);
        $projects = [];

        $allProjectsEstimateTime = 0;
        $allProjectsSpentTime = 0;

        foreach ($userProjects as $project) {
            $projectOut = Arr::only($project, ['id', 'path_with_namespace', 'description']);
            $projectIssues = $client->issues()->perPage(null)->all($project['id']);

            $timeDetails = [];
            $timeDetails['total_estimate'] = 0;
            $timeDetails['total_time_spent'] = 0;
            $daily = [];
            $monthly = [];

            foreach ($projectIssues as $issue) {
                $date = Carbon::parse($issue['created_at']);

                $timeEstimate = $issue['time_stats']['time_estimate'] / 60;
                $totalTimeSpent = $issue['time_stats']['total_time_spent'] / 60;

                self::setTimes($daily, $date->toDateString(), $timeEstimate, $totalTimeSpent);
                self::setTimes($monthly, $date->format('Y-m'), $timeEstimate, $totalTimeSpent);

                $allProjectsEstimateTime += $timeEstimate;
                $allProjectsSpentTime += $totalTimeSpent;

                $timeDetails['total_estimate'] += $timeEstimate;
                $timeDetails['total_time_spent'] += $totalTimeSpent;
            }

            $timeDetails['daily'] = $daily;
            $timeDetails['monthly'] = $monthly;

            $projectOut['time_details'] = $timeDetails;
            $projects[] = $projectOut;
        }

        return [
            'projects' => $projects,
            'all_projects_estimate_time' => $allProjectsEstimateTime,
            'all_projects_spent_time' => $allProjectsSpentTime
        ];
    }

    private static function setTimes(array &$array, string $dateKeyString, $timeEstimate, $totalTimeSpent)
    {
        if (isset($array[$dateKeyString])) {
            $array[$dateKeyString]['time_estimate'] += $timeEstimate;
            $array[$dateKeyString]['total_time_spent'] += $totalTimeSpent;
        } else {
            $array[$dateKeyString]['time_estimate'] = $timeEstimate;
            $array[$dateKeyString]['total_time_spent'] = $totalTimeSpent;
        }
    }
}
