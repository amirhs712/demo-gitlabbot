<?php

use App\Models\User;
use App\Scripts\Validation\CustomException;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * @param $path
 * @return string
 */
function sasset($path)
{
    return asset("storage/$path");
}

/**
 * Flash a message to the session
 * @param string $key
 * @param string $value
 */
function flash($key, $value)
{
    session()->flash($key, $value);
}

/**
 * @Description Get authenticated user
 * @param $guard
 * @return User|Authenticatable|null
 */
function user($guard = null)
{
    return Auth::guard($guard)->user();
}

/**
 * throw a CustomException.
 * @param int $status
 * @param string $message
 * @param array $data
 * @throws CustomException
 */

function cexception($status = 422, $message = '', $data = [])
{
    throw new CustomException($status, $message, $data);
}

function cexception_if(bool $condition = false, $status = 422, $message = '', $data = [])
{
    if ($condition) {
        throw new CustomException($status, $message, $data);
    }
}

function pluck($array, $field)
{
    $array = collect($array);
    return $array->pluck($field)->toArray();
}

/**
 * @param $query
 * @param $field
 * @return array|null|mixed
 */
function qpluck($query, $field)
{
    return $query->get($field)->pluck($field)->toArray();
}
