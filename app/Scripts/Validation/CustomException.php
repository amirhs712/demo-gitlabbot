<?php


namespace App\Scripts\Validation;

//Currently Only JSON API is supported.
use Exception;

/**
 * Class CustomException
 * @package App\Scripts\Validation
 */
class CustomException extends Exception
{
    public const UNPROCESSABLE_ENTITY = 422;
    public const GITLAB_TOKEN_NOT_SET = -100;
    public const INVALID_GITLAB_TOKEN = -101;
    public const FORBIDDEN_ACCESS_TO_GITLAB = -102;


    public $status;
    public $data;

    /**
     * CustomException constructor.
     * @param int $status
     * @param string $message
     * @param array $data
     */
    public function __construct($status = 422, $message = '', $data = [])
    {
        parent::__construct($message);
        $this->status = $status;
        $this->data = $data;
    }

}
