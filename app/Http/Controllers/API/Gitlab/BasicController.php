<?php

namespace App\Http\Controllers\API\Gitlab;

use App\Http\Controllers\Controller;
use App\Scripts\Enums\Arch;
use App\Scripts\Helpers\Project\APIHelper;
use Illuminate\Http\Request;

class BasicController extends Controller
{
    public function profile()
    {
        return Arch::api(1, '', [
            'profile' => user()->gitlab_profile
        ]);
    }

    public function projects()
    {
        $client = APIHelper::getClient();
        $projects = $client->projects()->all(['membership'=> true]);

        user()->update([
            'gitlab_projects' => $projects
        ]);

        return Arch::api(1, '', [
            'projects' => $projects
        ]);
    }

    public function issues()
    {
        $client = APIHelper::getClient();
        $issues = $client->issues()->all();

        user()->update([
            'gitlab_issues' => $issues
        ]);

        return Arch::api(1, '', [
            'issues' => $issues
        ]);
    }
}
