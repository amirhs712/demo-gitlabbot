<?php

namespace App\Http\Controllers\API\Gitlab;

use App\Http\Controllers\Controller;
use App\Scripts\Enums\Arch;
use App\Scripts\Helpers\Project\IssueTimeTrackingHelper;
use Illuminate\Http\Request;

class IssueTimeTrackingController extends Controller
{
    public function report()
    {
        return Arch::api(1, '',
            IssueTimeTrackingHelper::report()
        );
    }
}
