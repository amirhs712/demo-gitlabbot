<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Scripts\Enums\Arch;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

/**
 * Class AuthController
 * @mixin \Eloquent
 * @package App\Http\Controllers\API
 */
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $loginAndRegister = ['login', 'register'];
        $this->middleware('guest')->only($loginAndRegister);
        $this->middleware('auth')->except([...$loginAndRegister, 'refresh']);

        $this->middleware('throttle:30,1')->except($loginAndRegister);
        $this->middleware('throttle:10,1')->only($loginAndRegister);
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => nope()->stringOf(100)->get(1),
            'email' => nope()->email()->stringOf(100)->unique('users')->get(1),
            'password' => nope()->string(4, 12)->get(1),
        ]);

        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => nope()->email()->stringOf(100)->get(1),
            'password' => nope()->stringOf(20)->get(1)
        ]);

        if (!$token = auth()->attempt($credentials)) {
            return Arch::api(0);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return Arch::api(1, '', auth()->user());
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'name' => nope()->stringOf(100)->get(1),
            'gitlab_token' => nope()->stringOf(100)->get(-2),
        ]);

        $user = auth()->user();
        $user->name = $data['name'];
        if (isset($data['gitlab_token'])) {
            $user->gitlab_token = $data['gitlab_token'];
        }

        $user->update();

        return Arch::api(1, '', [
            'user' => $user
        ]);
    }

    public function logout()
    {
        auth()->logout();

        return Arch::api(1, '', ['message' => 'Successfully logged out']);
    }


    public function refresh()
    {
        try {
            if (!$user = \JWTAuth::parseToken()->authenticate()) {
                //Abort if not authenticated.
                abort(401);
            }

            $refreshed = auth()->refresh();
            auth()->logout();
        } catch (TokenExpiredException $e) {
            try {
                $refreshed = \JWTAuth::refresh(true);
            } catch (JWTException $e) {
                return Arch::api(0);
            }
        } catch (JWTException $e) {
            return Arch::api(0);
        }

        return $this->respondWithToken($refreshed);
    }

    protected function respondWithToken($token)
    {
        return Arch::api(1, '', [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
