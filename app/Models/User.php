<?php

namespace App\Models;

use App\Scripts\Helpers\Project\APIHelper;
use Carbon\Carbon;
use Gitlab\Client;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;

//use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @property $name
 * @property $email
 * @property $password
 * @property $gitlab_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property array $gitlab_profile
 * @property integer $gitlab_user_id
 * @property array $gitlab_projects
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
//        'name',
//        'email',
//        'password',
//        'gitlab_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'gitlab_token',
        'gitlab_profile',
        'gitlab_projects',
        'gitlab_issues',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
//        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();

    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getGitlabProfileAttribute()
    {
        if (isset($this->attributes['gitlab_profile'])) {
            $fetched_at = Carbon::parse($this->attributes['gitlab_profile']['fetched_at']);
            if (now()->diffInMinutes($fetched_at) < 5) {
                //Cache will be refreshed every 5 minutes.
                return $this->attributes['gitlab_profile'];
            }
        }

        $profile = APIHelper::getMe();
        $profile['fetched_at'] = now();

        $this->update([
            'gitlab_profile' => $profile
        ]);

        return $profile;
    }

    public function getGitlabUserIdAttribute()
    {
        return $this->gitlab_profile['id'];
    }
}
