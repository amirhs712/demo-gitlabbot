<?php

namespace App\Exceptions;

use App\Scripts\Enums\Arch;
use App\Scripts\Validation\CustomException;
use Gitlab\Exception\RuntimeException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use function Clue\StreamFilter\fun;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        CustomException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $ex) {

        });

        $this->reportable(function (RuntimeException $ex) {
            switch ($ex->getMessage()) {
                case '401 Unauthorized':
                {
                    $status = CustomException::INVALID_GITLAB_TOKEN;
                    break;
                }

                case 'Forbidden':
                {
                    $status = CustomException::FORBIDDEN_ACCESS_TO_GITLAB;
                    break;
                }

                default:
                    throw $ex;
            }

            cexception($status);
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof CustomException) {
            return $this->renderCustomException($e);
        }

        return parent::render($request, $e);
    }

    private function renderCustomException(CustomException $ex)
    {
        return Arch::api($ex->status, $ex->getMessage(), $ex->data);
    }

    private function renderGitlabException(Throwable $ex)
    {
        switch ($ex->getMessage()) {
            case '401 Unauthorized':
            {
                return Arch::api(CustomException::INVALID_GITLAB_TOKEN);
            }

            case 'Forbidden':
            {
                return Arch::api(CustomException::FORBIDDEN_ACCESS_TO_GITLAB);
            }
        }
    }
}
