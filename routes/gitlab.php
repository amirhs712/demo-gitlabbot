<?php

use App\Http\Controllers\API\Gitlab\BasicController;
use App\Http\Controllers\API\Gitlab\IssueTimeTrackingController;

Route::get('profile', [BasicController::class, 'profile']);
Route::get('projects', [BasicController::class, 'projects']);
Route::get('issues', [BasicController::class, 'issues']);

//-- IssueTimeTracking
Route::get('issues/time-tracking/report', [IssueTimeTrackingController::class, 'report']);
