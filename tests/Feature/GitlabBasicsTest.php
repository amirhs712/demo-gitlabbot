<?php

namespace Tests\Feature;

use App\Models\User;
use App\Scripts\Helpers\Project\APIHelper;
use App\Scripts\Helpers\Project\IssueTimeTrackingHelper;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\Text;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class GitlabBasicsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $gitlab_token = env('GITLAB_TEST_TOKEN');
        if (!$gitlab_token) {
            self::fail('Set GITLAB_TEST_TOKEN variable in your .env file before running tests.');
        }

        $this->defaultHeaders = [
            'Accept' => 'application/json'
        ];
    }

    public function testBasics()
    {
        $faker = Factory::create();
        $name = $faker->name;
        $email = $faker->unique()->safeEmail;
        $res = $this->post('api/auth/register', [
            'name' => $name,
            'email' => $email,
            'password' => 'abcde'
        ]);

        $res->assertJsonStructure([
            'status',
            'message',
            'data' => [
                'access_token',
                'token_type',
                'expires_in'
            ]
        ]);

        $token = $res->json('data.access_token');
        $this->withHeader('Authorization', "Bearer $token");

        //Set Gitlab API Token
        $name = "New $name";
        $res = $this->post('api/auth/update', [
            'name' => $name,
            'gitlab_token' => env('GITLAB_TEST_TOKEN')
        ]);
        $res->assertSuccessful();
        $userInfo = $res->json('data.user');
        self::assertEquals([
            'name' => $name,
            'email' => $email
        ], Arr::only($userInfo, ['name', 'email']));

        $user = User::whereEmail($email)->first();
        self::assertNotNull($user);
        $this->be($user);

        //Get Gitlab Profile
        //This will also ensure we can connect to gitlab.
        $res = $this->get('api/gitlab/profile');
        $res->assertJson([
            'data' => [
                'profile' => APIHelper::getMe()
            ]
        ]);

        //List issues
        $res = $this->get('api/gitlab/issues');
        $user->refresh();
        $res->assertJsonFragment([
            'data' => [
                'issues' => $user->gitlab_issues
            ]
        ]);

        //Test The Report
        $res = $this->get('/api/gitlab/issues/time-tracking/report');
        $res->assertJsonFragment([
            'data' => IssueTimeTrackingHelper::report()
        ]);

        //CleanUp
        $this->cleanUpUser($email);
    }

    private function cleanUpUser($email)
    {
        User::whereEmail($email)->delete();
    }
}
