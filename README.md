## GitlabBot

### Installation

This application runs on mongodb, make sure you have a running instance and set the configuration in your .env file.

```.dotenv
DB_CONNECTION=mongodb
DB_HOST=127.0.0.1
DB_PORT=27017
DB_DATABASE=GitlabBot
DB_USERNAME=
DB_PASSWORD=
```

Run

```cmd
composer install
```

```
php artisan migrate
```

### Usage

* Use a proxy/vpn in Iran, this application directly connects to gitlab.com
which has restricted access to iranians.

* This project uses JWT for authentication, after retrieving an `access_token`,
sent it as a `Bearer` token in `Authorization` header.

* Make sure to set your gitlab personal access token via `update` request.
### Testing

* Before running any tests make sure to provide `GITLAB_TEST_TOKEN` variable for testing inside
of `.env` file.
This token will be used for all the tests.
